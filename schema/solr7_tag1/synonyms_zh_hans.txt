# Contains synonyms to use for your index. For the format used, see
# http://wiki.apache.org/solr/AnalyzersTokenizersTokenFilters#solr.SynonymFilterFactory
# (Lines starting with a pound character # are ignored.)
# e² => e2 (Disabled, handled by accents.)
RSK => Renesas Starter Kit
RA => RA Arm
RZ => RZ Arm
RE => RE Cortex
RX => RX 32-bit
RH850 => RH850 Automotive
AI => RZ AI
PowerNavigator => Power Navigator
PowerCompass => Power Compass
e2studio => e2 studio
CCRX => CC-RX
CCRL => CC-RL
CCRH => CC-RH
