<?php
/*
 * Extract unique search keywords and no of search instances from SumoLogic logs
 */

// Sumologic Query to extract search keywords

// Query String
// ((((_source="Test" and _collector="Health Metrics"))))
// | where %"message.solr_response_time" > 0
// | where url matches "*/search?keywords=*"
// | where %"message.count" > 0
// | where !isEmpty(%"message.keywords")

// Exported columns
// _messagetimems	_messagetime	host	message.count	message.keywords	message.solr_response_time	url	_raw

// Run the query on SumoLogic and export the results as a CSV file. SumoLogic can not export more than 10,000 rows.
// Adjust the time period as necessary.

$off_host = 2;
$off_keywords = 4;

if (count($argv) == 1) {
    print("Usage: {$argv[0]} search-results-xxx.csv" . PHP_EOL);
    exit(1);
}

$file = $argv[1];


$count = 0;

$header = [];
$rows = [];
$keywords = [];

if (($fd = fopen($file, "r")) !== FALSE) {
    while (($data = fgetcsv($fd, 0, ",")) !== FALSE) {
        if ($count == 0) {
            $header = $data;
            $i = 0;
            foreach($header as $head) {
                switch($head) {
                    case 'host':
                        $off_host = $i;
                        break;
                    case 'message.keywords':
                        $off_keywords = $i;
                        break;
                }
                $i++;
            }
        } else {
            // use only data from www.renesas.com
            if ($data[$off_host] == 'www.renesas.com') {
                $keyword = $data[$off_keywords];
                if (!isset($keywords[$keyword])) {
                    $rows[] = $data;
                    $keywords[$keyword] = 1;
                } else {
                    $keywords[$keyword]++;
                }
            }
        }
        $count++;
    }
    fclose($fd);
}

print("Rows = " . count($rows) . " Total = $count " . PHP_EOL);
print("Unique keywords = " . count($keywords) . PHP_EOL);
asort($keywords);
print_r($keywords);
