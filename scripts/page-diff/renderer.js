const { ipcRenderer } = require('electron');

const replaceText = (selector, text) => {
    const element = document.getElementById(selector)
    if (element) element.innerText = text
}

window.addEventListener('DOMContentLoaded', () => {
    for (const dependency of ['chrome', 'node', 'electron']) {
        replaceText(`${dependency}-version`, process.versions[dependency])
    }
})

document.querySelector('form').addEventListener('submit', (event) => {
    event.preventDefault();   // stop the form from submitting
    let data = {
        src:document.getElementById("src").value,
        dest:document.getElementById("dest").value,
        region:document.getElementById("region").value,
        keywords:document.getElementById("keywords").value,
        link:document.getElementById("link").value
    }
    ipcRenderer.send('form:submit', data)
});

ipcRenderer.on("search:data", (event, data) => {
    console.log(data)

    document.getElementById("src").value = data.src.url
    document.getElementById("dest").value = data.dest.url
    let s = ""
    let i = 0;
    for (i = 0; i < data.tests.length; i++) {
         s = s + '<li>' + '<a href="#" data-ga="' + data.tests[i] + '">' + data.tests[i][1] + '</a></li>'
    }
    document.getElementById('tests').innerHTML = s
    for (i = 0; i < document.links.length; i++) {
        document.links[i].addEventListener('click', (event) => {
            console.log(event)
            let a = event.target
            const data_ga = a.getAttribute("data-ga").split(',')
            console.log(data_ga)
            document.getElementById("region").value = data_ga[0]
            document.getElementById("keywords").value = data_ga[1]
            document.getElementById("link").value = data_ga[2]
        })
    }
})