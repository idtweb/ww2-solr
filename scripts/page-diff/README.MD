# Page Diff
Desktop application to compare search differences between SOLR 4 and 7.

## Requirements
- node v10+
- npm v6+

## Build
**npm install**

## Run
Make changes to config.json to specify the source and destination servers and the list of tests. 

Each test line has 3 parts:
- region
- keywords
- expected link (typically found on src as the first result)

```
{
  "src": "https://www.renesas.com/",
  "dest": "https://idtweb:lowjitter@www-idtstg-www.idt.com/",
  "tests" : [
    ["us/en", "flash programmer", "/software-tool/renesas-flash-programmer-programming-gui"],
    ["us/en", "RT plastic", "/products/space-harsh-environment/rad-tolerant-plastic"],
    ["us/en", "isim:pe", "/software-tool/isimpe-offline-simulation-tool"],
    ["us/en", "e2 lite", "/software-tool/e2-emulator-lite-rte0t0002lkce00000r"],
    ["us/en", "cs+", "/software-tool/cs"],
    ["us/en", "rl78", "/products/microcontrollers-microprocessors/rl78-low-power-8-16-bit-mcus"],
    ["us/en", "bluetooth", "/products/interface-connectivity/wireless-communications/bluetooth-low-energy-modules"],
    ["us/en", "ra nomenclature", "/document/gde/ra-family-nomenclature"],
    ["us/ja", "スマコン", "/software-tool/smart-configurator"],
    ["us/ja", "e2エミュレータ", "/software-tool/e2-emulator-rte0t00020kce00000r"]
  ]
}
```

**npm start**

This should open up the main window, with the test links listed. 

Enter the region, keywords and run the page diff. Or click on one of the links, to load the test into the form and then run it.
