const { app, BrowserWindow, ipcMain } = require('electron');
const fs = require('fs');
const path = require('path');
const os = require('os')

app.commandLine.appendSwitch('ignore-certificate-errors')

let mainWin, win1, win2;
let windows = new Set()

let config = JSON.parse(fs.readFileSync('config-ww2-2407.json', 'utf-8'));

// modify your existing createWindow() function
const createMainWindow = () => {
    const win = new BrowserWindow({
        x:0,
        y:0,
        width: 400,
        height: 200,
        webPreferences: {
            nodeIntegration: true,
            contextIsolation: false,
            preload: path.join(__dirname, 'preload.js')
        }
    })

    win.loadFile('index.html').then(() => {
        // win.openDevTools()
        win.send("search:data", config)
    })

    return win;
}

// modify your existing createWindow() function
const createWindow = () => {
    let x, y;

    x = 500; y = 0;
    if (windows.size > 0) {
        x = 1780
    }
    // const currentWindow = BrowserWindow.getFocusedWindow();
    // if (currentWindow) {
    //     const [ currentWindwoX, currentWindwoY ] = currentWindow.getPosition();
    //     x = currentWindwoX + 1280;
    //     y = currentWindwoY + 0;
    // }
    const win = new BrowserWindow({
        x : x,
        y : y,
        width: 1280,
        height: 1200,
        webPreferences: {
            preload: path.join(__dirname, 'preload.js')
        }
    })

    windows.add(win)

    return win;
}

// ...
app.whenReady().then(() => {

    mainWin = createMainWindow()

    app.on('activate', () => {
        if (BrowserWindow.getAllWindows().length === 0) createMainWindow()
    })
})

app.on('window-all-closed', () => {
    if (process.platform !== 'darwin') app.quit()
})

ipcMain.on('form:submit', (event, data) => {
    console.log(data)

    if (win1 == null) {
        win1 = createWindow()
        // win1.openDevTools()
    }
    win1.loadURL(data.src + data.region + '/search?keywords=' + data.keywords).then(() => {
        win1.setTitle(config.src.name + " | " + win1.getTitle())
        if (data.link.length > 0) {
            win1.send("find:link", '/' + data.region + data.link)
        }
    })

    if (win2 == null) {
        win2 = createWindow()
        // win2.openDevTools()
    }
    win2.loadURL(data.dest + data.region + '/search?keywords=' + data.keywords).then(() => {
        win2.setTitle(config.dest.name + " | " + win2.getTitle())
        if (data.link.length > 0) {
            win2.send("find:link", '/' + data.region + data.link)
        }
    })
});
